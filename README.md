Introduction
============
Within this archive you find the replication package 
for the article "On the Replicability and Reproducibility of Deep Learning in Software Engineering" 
by Chao Liu, Cuiyun Gao, Xin Xia, David Lo, John Grundy, and Xiaohu Yang 
for currently under review at ACM Transactions on Software Engineering and Methodology. 


Contents
============
literature_review.xlsx provides the collected data for the reviewed studies.

The folders ASTNN, DeepCS, RRGen, and RLNN are the replication packages for our used four deep learning models.
The README file in each folder shows how to run model respectively.